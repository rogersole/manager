<?php


?>



<!DOCTYPE html>
<html lang="es">
<head>
    <title>Publisher Stats by date</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->

    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
    <script src="https://www.amcharts.com/lib/4/lang/de_DE.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script src="http://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js" type="text/javascript"></script>
</head>
<body>
<!--MENÚ SUPERIOR-->

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #fdd400">
    <a class="navbar-brand">KingMonetize Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Dashboard</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="by_publisher.php">by Publisher</a>
                    <a class="dropdown-item" href="by_tag.php">by Tag</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="tagsbyfeed.php">Tags by Feed</a>
                    <a class="dropdown-item" href="feedsbytag.php">Feeds by Tag</a>
                    <a class="dropdown-item" href="subid_optimization.php">Subid optimization by Feed</a>
                    <a class="dropdown-item" href="country_optimization.php">Country optimization by Feed</a>
                    <a class="dropdown-item" href="domains.php">Domains and Apps</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publishers</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="new_publisher.php">New Publisher</a>
                    <a class="dropdown-item" href="new_tag.php">New Tag</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="publisher_list.php">Publishers</a>
                    <a class="dropdown-item" href="tag_list.php">Tags</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Advertisers</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="new_advertiser.php">New Advertiser</a>
                    <a class="dropdown-item" href="new_feed.php">New Feed</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="advertisers_list.php">Advertisers</a>
                    <a class="dropdown-item" href="feeds_list.php">Feeds</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Infraestructure</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="droplets.php">Droplets</a>
                    <a class="dropdown-item" href="report_infr.php">Report Cost</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Finance</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="adv_balance.php">Advertisers API Numbers</a>
                    <a class="dropdown-item" href="adv_balance.php">Feed Discounts</a>
                    <a class="dropdown-item" href="adv_balance.php">Advertisers Balance</a>
                </div>
            </li>
        </ul>
        </li><a class="nav-link" href="https://www.kingmonetize.com/documentation">Documentation</a> <a class="btn btn-light" style="margin-left: 10px" href="logout.php" role="button">Logout</a>
    </div>
</nav>


<!--ESPACIO ANTES DEL MAIN CONTENT-->
<div class="row my-1">
    <div class="col text-center">
        <h2>Publisher Stats by Date (UTC)</h2>
    </div>
</div>

<!--EMPIEZA EL MAIN CONTENT-->

<!--ESPACIO ANTES DEL MAIN CONTENT-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form name="DateFilter" method="post">
                <div class="row" style="margin-left: 100px">
                    <div class="col-md-auto">
                        <input type="text" class="form-control" name="publisher_id" placeholder="Publisher ID"/>
                    </div>
                    <div class="col-md-auto">
                        <input type="text" class="form-control" name="publisher_name" placeholder="Publisher Name"/>
                    </div>
                    <div class="col-md-auto">
                        <input type="date" class="form-control" name="date_start" required/>
                    </div>
                    <div class="col-md-auto">
                        <input type="date" class="form-control" name="date_end" required/>
                    </div>
                    <div class="col-md-auto">
                        <input type="submit" name="submit" value="Report" class="btn" style="background: gold"></input>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 10px">

    <h2 class="text-center"><span class="badge badge-light">Actual Date Range: <?php echo $date_start;?> to <?php echo $date_end;?></span></h2>
    <!--TABLA-->
    <table class="table table-responsive table-bordered" style="margin-top: 10px">
        <thead>
        <tr>
            <th scope="col" style="width: 16%">Date</th>
            <th scope="col" style="width: 11%">Impressions</th>
            <th scope="col">Valid Impressions</th>
            <th scope="col">Fallbacks</th>
            <th scope="col">CPM</th>
            <th scope="col">Revenue</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($totals_results as $row) {
            echo "<tr>
                        <td>".str_replace('00:00:00','',$row['date'])."</td>
                        <td>".number_format($row['impressions'],0,',','.')."</td>
                        <td>".number_format($row['valid_impressions'],0,',','.')."</td>
                        <td>".number_format($row['fallbacks'],0,',','.')."</td>
                        <td>".number_format($row['cpm'],2,',','.')."</td>
                        <td>$".number_format($row['revenue'],2,',','.')."</td>
                    </tr>";
        }
        ?>
        </tbody>
    </table>
    <table class="table table-responsive table-bordered" style="margin-top: 1px">
        <thead>
        <tr>
            <th scope="col">---</th>
            <th scope="col" style="width: 11%">Impressions</th>
            <th scope="col">Valid Impressions</th>
            <th scope="col">Fallbacks</th>
            <th scope="col">CPM</th>
            <th scope="col">Revenue</th>
        </tr>
        </thead>
        <tbody>
        <?php
        echo "<tr>
                        <td>Totals</td>
                        <td>".number_format($total_impressions,0,',','.')."</td>
                        <td>".number_format($total_postbacks,0,',','.')."</td>
                        <td>".number_format($total_under_validation_impressions,0,',','.')."</td>
                        <td>".number_format($total_validated_impressions,0,',','.')."</td>
                        <td>$".number_format($total_revenue,2,',','.')."</td>
                    </tr>";
        ?>
        </tbody>
    </table>
</div>











<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>




</body>
</html>