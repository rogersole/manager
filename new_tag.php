<?php
require "classes/db_management.php";

$db_class = new  db_management();

if (!empty($_POST)){
    $tag_id = $_POST['tag_id'];
    $tag_name = $_POST['tag_name'];
    $publisher_id = $_POST['publisher_id'];
    if ($_POST['mainstream'] == 0){$traffic_type = 0;};
    if ($_POST['adult'] == 1){$traffic_type = 1;};
    if ($_POST['both'] == 2){$traffic_type = 2;};
}


$query="INSERT INTO tags (tag_id,tag_name,publisher_id,traffic_type) VALUES ('$tag_id','$tag_name','$publisher_id','$traffic_type')";

$conn = $db_class->operation_db_connection();

$query1 = pg_query($conn, $query);

pg_close($conn);

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>1 New Tag</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->

</head>
<body>
<!--MENÚ SUPERIOR-->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #fdd400">
    <a class="navbar-brand">KingMonetize Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Dashboard</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="by_publisher.php">by Publisher</a>
                    <a class="dropdown-item" href="by_tag.php">by Tag</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="tagsbyfeed.php">Tags by Feed</a>
                    <a class="dropdown-item" href="feedsbytag.php">Feeds by Tag</a>
                    <a class="dropdown-item" href="subid_optimization.php">Subid optimization by Feed</a>
                    <a class="dropdown-item" href="country_optimization.php">Country optimization by Feed</a>
                </div>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publishers</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="new_publisher.php">New Publisher</a>
                    <a class="dropdown-item active" href="new_tag.php">New Tag</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="publisher_list.php">Publishers</a>
                    <a class="dropdown-item" href="tag_list.php">Tags</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Advertisers</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="new_advertiser.php">New Advertiser</a>
                    <a class="dropdown-item" href="new_feed.php">New Feed</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="advertisers_list.php">Advertisers</a>
                    <a class="dropdown-item" href="feeds_list.php">Feeds</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Infraestructure</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="droplets.php">Droplets</a>
                    <a class="dropdown-item" href="report_infr.php">Report Cost</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Finance</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="adv_balance.php">Advertiser Balance</a>
                </div>
            </li>
        </ul>
        </li><a class="nav-link" href="skype:live:publishers_58?chat">Support</a> <a class="btn btn-light" style="margin-left: 10px" href="logout.php" role="button">Logout</a>
    </div>
</nav>

<div class="container">
<div class="dropdown-divider" style="margin-top: 8px"></div>
    <div class="col-md-12">
            <div class="container" style="margin-top: 10px">
                <h2 class="text-center"><span class="badge badge-light">New Tag</span></h2>
                <!--TABLA-->
                <form name="new_publisher" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tag ID</label>
                        <input type="text" class="form-control" name="tag_id" aria-describedby="emailHelp" placeholder="Tag ID" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tag Name</label>
                        <input type="text" class="form-control" name="tag_name" aria-describedby="emailHelp" placeholder="Tag Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Publisher ID</label>
                        <input type="text" class="form-control" name="publisher_id" aria-describedby="emailHelp" placeholder="Publisher ID" required>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="mainstream" value="0"> Mainstream
                    </div>
                    <div class="form-group" style="margin-top:10px">
                        <input type="checkbox" name="adult" value="1"> Adult
                    </div>
                    <div class="form-group" style="margin-top:10px">
                        <input type="checkbox" name="both" value="2"> Both
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>




</body>
</html>