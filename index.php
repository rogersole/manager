<?php

require "classes/db_management.php";

$db_class1 = new db_management();
$db_class = $db_class1->getactualmonthtotalsnetwork();


$requests = $db_class['requests'];
$impressions = $db_class['impressions'];
$pub_revenue = $db_class['pub_revenue'];
$adv_bid = $db_class['adv_bid'];
$margin = $db_class['margin'];
$filtered = $db_class['filtered'];
$cpm = $db_class['cpm'];
$postbacks = $db_class['postbacks'];
$fallbacks = $db_class['fallbacks'];

?>



<!DOCTYPE html>
<html lang="es">
<head>
    <title>Dashboard</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->

    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
    <script src="https://www.amcharts.com/lib/4/lang/de_DE.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script src="http://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js" type="text/javascript"></script>
</head>
<body>
<!--MENÚ SUPERIOR-->

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #fdd400">
    <a class="navbar-brand">KingMonetize Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Dashboard</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="by_publisher.php">by Publisher</a>
                    <a class="dropdown-item" href="by_tag.php">by Tag</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="tagsbyfeed.php">Tags by Feed</a>
                    <a class="dropdown-item" href="feedsbytag.php">Feeds by Tag</a>
                    <a class="dropdown-item" href="subid_optimization.php">Subid optimization by Feed</a>
                    <a class="dropdown-item" href="country_optimization.php">Country optimization by Feed</a>
                    <a class="dropdown-item" href="domains.php">Domains and Apps</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publishers</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="new_publisher.php">New Publisher</a>
                    <a class="dropdown-item" href="new_tag.php">New Tag</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="publisher_list.php">Publishers</a>
                    <a class="dropdown-item" href="tag_list.php">Tags</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Advertisers</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="new_advertiser.php">New Advertiser</a>
                    <a class="dropdown-item" href="new_feed.php">New Feed</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="advertisers_list.php">Advertisers</a>
                    <a class="dropdown-item" href="feeds_list.php">Feeds</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Infraestructure</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="droplets.php">Droplets</a>
                    <a class="dropdown-item" href="report_infr.php">Report Cost</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Finance</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="adv_api_numbers.php">Advertisers API Numbers</a>
                    <a class="dropdown-item" href="adv_balance.php">Feed Discounts</a>
                    <a class="dropdown-item" href="adv_balance.php">Advertisers Balance</a>
                </div>
            </li>
        </ul>
        </li><a class="nav-link" href="skype:live:publishers_58?chat">Documentation</a> <a class="btn btn-light" style="margin-left: 10px" href="logout.php" role="button">Logout</a>
    </div>
</nav>


<!--ESPACIO ANTES DEL MAIN CONTENT-->
<div class="row my-1">
    <div class="col text-center">
        <h2>Network Monthly Stats</h2>
    </div>
</div>

<!--EMPIEZA EL MAIN CONTENT-->
<div class="container" style="margin-top: 15px">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Requests</span></h2>
                <h1 class="text-center"><?php echo number_format($requests,0,',','.'); ?></h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Impressions</span></h2>
                <h1 class="text-center"><?php echo number_format($impressions,0,',','.'); ?></h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Postbacks</span></h2>
                <h1 class="text-center"><?php echo number_format($postbacks,0,',','.'); ?></h1>
            </div>
        </div>
    </div>
    <!--/row-->
</div>
<!--container-->
<div class="container" style="margin-top: 10px">
    <div class="row">
        <div class="col-3">
        </div>
    </div>
    <!--/row-->
</div>
<!--container-->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Cost</span></h2>
                <h1 class="text-center">$<?php echo number_format($pub_revenue,2,',','.'); ?></h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Margin</span></h2>
                <h1 class="text-center">$<?php echo number_format($margin,2,',','.');?></h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Revenue</span></h2>
                <h1 class="text-center">$<?php echo number_format($adv_bid,2,',','.'); ?></h1>
            </div>
        </div>
    </div>
</div>
<!--container-->
<div class="container" style="margin-top: 10px">
    <div class="row">
        <div class="col-3">
        </div>
    </div>
    <!--/row-->
</div>
<!--container-->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Fallbacks</span></h2>
                <h1 class="text-center"><?php echo number_format($fallbacks,0,',','.'); ?></h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">CPM</span></h2>
                <h1 class="text-center">$<?php echo number_format($cpm,2,',','.');?></h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Filtered</span></h2>
                <h1 class="text-center"><?php echo number_format($filtered,0,',','.'); ?></h1>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-top: 15px">
            <div class="card card-body">
                <h2 class="text-center"><span class="badge badge-light">Revenue by Date</span></h2>
                <!-- Styles -->
                <style>
                    #chartdiv2 {
                        width: 100%;
                        height: 500px;
                    }
                </style>
                <!-- Chart code -->
                <script>
                    am4core.ready(function() {
// Themes begin
                        am4core.useTheme(am4themes_animated);
// Themes end
// Create chart instance
                        var chart = am4core.create("chartdiv2", am4charts.XYChart);
// Export
                        chart.exporting.menu = new am4core.ExportMenu();

                        /* Create axes */
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "day";
                        categoryAxis.renderer.minGridDistance = 30;

                        /* Create value axis */
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                        /* Create series */
                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                        columnSeries.name = "revenue";
                        columnSeries.dataFields.valueY = "revenue";
                        columnSeries.dataFields.categoryX = "day";

                        columnSeries.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
                        columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
                        columnSeries.columns.template.propertyFields.stroke = "stroke";
                        columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
                        columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
                        columnSeries.tooltip.label.textAlign = "middle";

                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                        lineSeries.name = "revenue";
                        lineSeries.dataFields.valueY = "revenue";
                        lineSeries.dataFields.categoryX = "day";

                        lineSeries.stroke = am4core.color("#fdd400");
                        lineSeries.strokeWidth = 3;
                        lineSeries.propertyFields.strokeDasharray = "lineDash";
                        lineSeries.tooltip.label.textAlign = "middle";

                        var bullet = lineSeries.bullets.push(new am4charts.Bullet());
                        bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
                        bullet.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY} $[/] [#fff]{additional}[/]"
                        var circle = bullet.createChild(am4core.Circle);
                        circle.radius = 4;
                        circle.fill = am4core.color("#fff");
                        circle.strokeWidth = 3;
                        chart.dataSource.url = "https://manager.kingmonetize.com/classes/monthly_chart_data_network.php";
                    }); // end am4core.ready()
                </script>

                <!-- HTML -->
                <div id="chartdiv2"></div>
            </div>
        </div>
    </div>
</div>









<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>




</body>
</html>