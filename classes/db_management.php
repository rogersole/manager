<?php


class db_management
{
    public function getactualmonthtotalsnetwork()
    {

        date_default_timezone_set('UTC');
        $hoy = date('Y-m-d'); //01-12-2001
        $dia = date('d');
        $mes = date('m');
        $ano = date('Y');
        $hora_actual = date('H'); //17

        $query = "select sum(impressions) as impressions, sum(pub_payout) as pub_revenue, sum(adv_bid) as adv_bid, sum(adv_bid)-sum(pub_payout) as margin, sum(pub_payout)/sum(impressions)*1000 as cpm, sum(filtered) as filtered, sum(fallbacks) as fallbacks
                from reporting where ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:00:00.000000'";
        $conn = $this->reporting_db_connection();
        $query1 = pg_query($conn, $query);

        while($row1 = pg_fetch_assoc($query1)){
            $impressions = $row1['impressions'];
            $pub_revenue = $row1['pub_revenue'];
            $adv_bid = $row1 ['adv_bid'];
            $margin = $row1['margin'];
            $cpm = $row1['cpm'];
            $filtered = $row1['filtered'];
            $fallbacks = $row1['fallbacks'];
        }

        //** REQUESTS */

        $query = "select sum(requests) as requests from reporting_requests where ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:00:00.000000'";

        $query2 = pg_query($conn, $query);

        while($row4 = pg_fetch_assoc($query2)){
            $requests = $row4['requests'];
        }

        //** CALCULO POSTBACKS */


        $query = "select count(postback_amount) as postbacks from reporting_postbacks where ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:59:59.000000'";

        $query3 = pg_query($conn, $query);

        while($row4 = pg_fetch_assoc($query3)){
            $postbacks = $row4['postbacks'];
        }

        pg_close($conn);

        $monthly_results = array(
            'requests' => $requests,
            'impressions' => $impressions,
            'pub_revenue' => $pub_revenue,
            'adv_bid' => $adv_bid,
            'margin' => $margin,
            'cpm' => $cpm,
            'filtered' => $filtered,
            'postbacks' => $postbacks,
            'fallbacks' => $fallbacks
        );

        return $monthly_results;
    }

    public function reporting_db_connection(){
        $dbconn_reporting = pg_connect("host= db.p.reporting.kingmonetize.com port=5432 dbname=reporting user=postgres password=toor");
        if (!$dbconn_reporting) {
            $error = "Error de conexión con PostgreSQL";
            file_put_contents("/tmp/logs/error.log", $error, FILE_APPEND);
            header("HTTP/1.0 502 Internal Services Error");
        }
        return $dbconn_reporting;
    }

    public function operation_db_connection(){
        $dbconn_reporting = pg_connect("host= db.p.operations.kingmonetize.com port=5432 dbname=postgres user=postgres password=toor");
        if (!$dbconn_reporting) {
            $error = "Error de conexión con PostgreSQL";
            file_put_contents("/tmp/logs/error.log", $error, FILE_APPEND);
            header("HTTP/1.0 502 Internal Services Error");
        }
        return $dbconn_reporting;
    }


}