<?php

require "db_management.php";

// Connect to MySQL
$db_class = new db_management();

date_default_timezone_set('UTC');
$hoy = date('Y-m-d'); //01-12-2001
$dia = date('d');
$mes = date('m');
$ano = date('Y');
$hora_actual = date('H'); //17

$conn = $db_class->reporting_db_connection();
// Fetch the data
$query = "
  SELECT  EXTRACT(DAY FROM ts) as day, sum(adv_bid) as revenue
  FROM reporting where ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:00:00.000000'
  group by 1 order by 1";
$query_exec = pg_query($conn, $query);

// All good?
if ( !$query_exec ) {
    // Nope
    $message  = 'Invalid query: ' . $query_exec->error . "n";
    $message .= 'Whole query: ' . $query;
    die( $message );
}

    $data[] = array();
    // Print out rows
    while ($row = pg_fetch_assoc($query_exec)) {
        $data[] = $row;
    }

    header( 'Content-Type: application/json' );
    echo json_encode($data);
